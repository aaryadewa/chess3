Chs.Piece = function(selectedPiece) {
	var create,
	
		getCell, getPiece, getId,
		getType, getColor,
		getRow, getCol, setRow, setCol,
		setHighlight, canMoveTo,
		isActive,
		
		piece = null, cell = null,
		data = null,
		id = '',
		active = false;
		
	create = function(selectedPiece) {
		piece = selectedPiece;
		cell = piece.parent();
		
		data = piece.data('piece');
		
		// set defaults
		id = selectedPiece.attr('id');
		
		setHighlight(true);
		return this;
	};
	
	getCell = function() {
	    return cell;
    };
    
    getPiece = function() {
        return piece;
    };
    
	isActive = function() {
	    return active; 
	};
	
	getType = function() {
	    return data.type;
    };
    
	getColor = function() {
	    return data.color;
	};
	
	getRow = function() {
	    return data.row;
    };
    
    setRow = function(row) {
        data.row = row;
	};
	
    getCol = function() {
        return data.col;
    };
    
	setCol = function(col) {
	    data.col = col;
    };
    
	setHighlight = function(highlighted) {
		if (highlighted) {
			piece.addClass('move');
			active = true;
		} else {
			piece.removeClass('move');
			active = false;
		}
	};
	
	canMoveTo = function(newCell) {
	    var map = Chs.Main.getMap(),
	        pieceColor = getColor(),
			rowFrom = getRow(),
	        colFrom = getCol(),
	        rowTo = newCell.getRow(),
	        colTo = newCell.getCol(),
			mapCellColor = map[rowTo][colTo],
			mapPieceColor = 1,
	        canMove = false;
			
		if (pieceColor == 'black')
			mapPieceColor = 2;
	        
        switch (getType()) {
            case 'pawn':
                var canStepForward = rowFrom > rowTo,
                    isBlocked = false,
                    isOneStepForward = rowFrom-1 == rowTo,
                    isInPlayerZone = rowTo > 3;
                    
                if (pieceColor == 'black') {
                    canStepForward = rowFrom < rowTo;
                    isOneStepForward = rowFrom+1 == rowTo,
                    isInPlayerZone = rowTo < 4;
                }
                
                for (var b = rowFrom-1; b >= rowTo; --b) {
                    if (map[b][colFrom] > 0) {
                        console.log('is blocked at '+b+'_'+colFrom);
                        isBlocked = true;
                        break;
                    }
                }
                
                if (canStepForward) {
                    if (colFrom == colTo) { // piece is go forward
                        if (mapCellColor == 0 && !isBlocked) {
                            if (isInPlayerZone) {
                                canMove = true;
                            } else {
                                if (isOneStepForward) {
                                    canMove = true;
                                }
                            }
                        }
                    }
                    
                    if (isOneStepForward) {
                        // do a diagonal movement
                        if (colFrom-1 == colTo || colFrom+1 == colTo) {
                            // capture the opponent's piece
                            if (mapCellColor > 0 && mapPieceColor != mapCellColor) {
                                canMove = true;
                            }
                        }
                    }
                }
                break;
				
			case 'rook':
				var legalMoves = (rowFrom == rowTo || colFrom == colTo);
				break;
				
			case 'knight':
				
				break;
            
            default:
                console.log('not supported yet');
                break;
        }
        
        return canMove;
    };
	
	return {
		create: create,
		getCell: getCell,
		getPiece: getPiece,
		isActive: isActive,
		getType: getType,
		getColor: getColor,
		getRow: getRow,
		setRow: setRow,
		getCol: getCol,
		setCol: setCol,
		setHighlight: setHighlight,
		canMoveTo: canMoveTo
	};
};
