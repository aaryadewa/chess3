var Chs = {};
Chs.Main = function() {
    var initBoard,
		putPiece, getMap, getMapNumRows, getMapNumCols,
		updateMap, viewMap,
	
        map = [
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ],
        mapNumRows = map.length,
		mapNumCols = mapNumRows;
        
    initBoard = function() {
        var board = $('#board');
            
        for (var r = 0; r < mapNumRows; r++) {
            var row = $('<div>', {
                id: 'row_' +r,
                class: 'row'
            });
        
            for (var c = 0; c < 8; c++) {
                var block = $('<div>', {
                    id: 'cell_' +r+ '_' +c,
                    class: 'cell'
                });
                block.data('cell', {
                    row: r,
                    col: c
                });
                
                if (r == 0 || r == 1 || r == 6 || r == 7) {
                    block.html(putPiece(r, c));
                }
                    
                row.append(block);
            }
            
            board.append(row);
        }
    };
    
    putPiece = function(row, col) {
        var pieceColor = 'black',
            pieceType = 'pawn',
            obj;
        
        if (row == 6 || row == 7) pieceColor = 'white';
        
        if (row == 0 || row == 7) {
            if (col == 0 || col == 7) pieceType = 'rook';
            else if (col == 1 || col == 6) pieceType = 'knight';
            else if (col == 2 || col == 5) pieceType = 'bishop';
            else if (col == 3) pieceType = 'queen';
            else if (col == 4) pieceType = 'king';
        }
        
        obj = $('<div>', {
            id: pieceType+ '_' +pieceColor+ '_' +row+ '_' +col,
            class: 'piece ' +pieceType+ ' ' +pieceColor,
            draggable: true
        });
        obj.data('piece', {
            type: pieceType,
            color: pieceColor,
            row: row,
            col: col
        });
        
        return obj;
    };
	
	updateMap = function(rowFrom, colFrom, rowTo, colTo, value) {
	    map[rowFrom][colFrom] = 0;
	    map[rowTo][colTo] = value;
	};
	
	viewMap = function(formatColor) {
	    for (var r = 0; r < mapNumRows; r++) {
	        var row = [];
	        for (var c = 0; c < mapNumCols; c++) {
	            var item = map[r][c],
	                color = '',
	                piece = item;
                
                if (formatColor) {
                    if (item > 0) {
                        if (item == 1)
                            color = 'white';
                        else if (item == 2)
                            color = 'black';
                    }
                    piece = color;
                }
                
                row.push(piece);
	        }
	        console.log(row);
	    }
	};
	
	getMap = function() { return map; };
	getMapNumRows = function() { return mapNumRows; };
	getMapNumCols = function() { return mapNumCols; };
    
    return {
        init: initBoard,
		updateMap: updateMap,
		viewMap: viewMap,
		getMap: getMap,
		getMapNumRows: getMapNumRows,
		getMapNumCols: getMapNumCols
    };
}();

$(Chs.Main.init);
