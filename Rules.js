Chs.Rules = function($) {
    var initRules,
        
        // assign piece movement rules
        pieceAssignMovementRules,
        pieceAssignCaptureRules,
    
        // the event handlers
        startMovePiece, finishMovePiece,
        enterDropZone, overDropZone,
        leaveDropZone, dropOntoDropZone,
        selectItem,
        
        // default and temporary variables
        playerTurn = 'white',
        blockScopes = [],
        
        // temporarily store first column here when start dragging a piece
        cell = null,
        
        // temporarily store the selected piece here
        piece = null, pieceMoved = null;
    
    startMovePiece = function(e) {
        var ele = e.target;
        if ($(ele).hasClass('piece')) {
            blockStart = ele.parentNode;
            blockStart.classList.add('start');
            ele.classList.add('move');
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/plain', ele.id);
            console.log('start dragging ' +ele.className);
        }
    };
    
    finishMovePiece = function(e) {
        var ele = e.target;
        if ($(ele).hasClass('piece')) {
            blockStart.classList.remove('start');
            blockStart = null;
            ele.classList.remove('move');
            console.log('finish dragging ' +ele.className);
        }
    };
    
    enterDropZone = function(e) {
        var ele = e.target;
        if (ele.className == 'column') {
            if (ele.id != blockStart.id) {
                ele.classList.add('over');
                e.preventDefault();
            }
        }
    };
    
    leaveDropZone = function(e) {
        var ele = e.target;
        if ($(ele).hasClass('column')) {
            ele.classList.remove('over');
            e.preventDefault();
        }
    };
    
    overDropZone = function(e) {
        var ele = e.target;
        if ($(ele).hasClass('column')) {
            if (ele.id != blockStart.id) {
                e.dataTransfer.dropEffect = 'move';
                e.preventDefault();
            }
        }
    };
    
    dropOntoDropZone = function(e) {
        var ele = e.target;
        if ($(ele).hasClass('column')) {
            if (ele.id != blockStart.id) {
                var pieceId = e.dataTransfer.getData('text/plain');
                ele.appendChild(document.getElementById(pieceId));
                ele.classList.remove('over');
            }
        }
        e.preventDefault();
    };
    
    selectItem = function(e) {
        var ele = $(e.target);
        if (ele.hasClass('piece')) {
            var pc = new Chs.Piece(),
				cl = new Chs.Cell();
				
			if (piece != null && cell != null) {
				piece.setHighlight(false);
				cell.setHighlight(false);
			}
			
			piece = pc.create(ele);
			cell = cl.create(piece.getCell());
        } else if (ele.hasClass('cell')) {
			if (piece != null && cell != null) {
			    var clTo = new Chs.Cell(),
			        cellTo = clTo.create(ele);
			        
			    if (piece.canMoveTo(cellTo)) {
			        var mapPieceColor = (piece.getColor() == 'white')? 1: 2;
			        
			        piece.setRow(cellTo.getRow());
			        piece.setCol(cellTo.getCol());
			        ele.html('');
			        ele.append(piece.getPiece());
			        Chs.Main.updateMap(cell.getRow(), cell.getCol(), cellTo.getRow(), cellTo.getCol(), mapPieceColor);
			    }
		        
				piece.setHighlight(false);
				cell.setHighlight(false);
				cellTo.setHighlight(false);
			}
        }
    };
    
    initRules = function() {
        var board = document.getElementById('board');
        
        board.addEventListener('dragstart', startMovePiece);
        board.addEventListener('dragend', finishMovePiece);
        board.addEventListener('dragenter', enterDropZone);
        board.addEventListener('dragover', overDropZone);
        board.addEventListener('dragleave', leaveDropZone);
        board.addEventListener('drop', dropOntoDropZone);
        board.addEventListener('click', selectItem);
    };
    
    return {
        init: initRules
    };
}(jQuery);

$(Chs.Rules.init);
