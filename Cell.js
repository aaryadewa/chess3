Chs.Cell = function() {
	var create,
		
		cell = null, piece = null,
		data = null,
		active = false,
		
		setHighlight,
		
		getCell, getPiece,
		getRow, getCol;
	
	create = function(selectedCell) {
		cell = selectedCell;
		piece = cell.children();
		
		data = cell.data('cell');
		setHighlight(true);
		return this;
	};
	
	getPiece = function() {
	    return piece;
	};
	
	getRow = function() {
	    return data.row;
	};
	
	getCol = function() {
	    return data.col;
    };
    
	setHighlight = function(highlighted) {
		if (highlighted) {
			cell.addClass('start');
			active = true;
		} else {
			cell.removeClass('start');
			active = false;
		}
	};
	
	return {
		create: create,
		getPiece: getPiece,
		getRow: getRow,
		getCol: getCol,
		setHighlight: setHighlight
	};
};
